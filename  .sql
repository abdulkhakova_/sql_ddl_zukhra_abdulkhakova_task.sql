CREATE TABLE Medications (
    medication_id (INT, PRIMARY KEY),
    name (VARCHAR(100), NOT NULL),
    description (VARCHAR(255)),
    manufacturer (VARCHAR(100)) ,
    price (DECIMAL(10,2), NOT NULL) ,
Constraints:
    price >= 0 (CHECK constraint) ,
);
CREATE TABLE Patients (
    patient_id (INT, PRIMARY KEY) ,
    first_name (VARCHAR(50), NOT NULL) ,
    last_name (VARCHAR(50), NOT NULL) ,
    gender (CHAR(1)) ,
ALTER TABLE Patients ADD date_of_birth (DATE, NOT NULL)
Constraints:
    date_of_birth >= '2000-01-01' (CHECK constraint) ,
    gender IN ('M', 'F') (CHECK constraint) ,
) ;
CREATE TABLE Prescriptions (
    prescription_id (INT, PRIMARY KEY),
    patient_id (INT, FOREIGN KEY references Patients(patient_id)),
    medication_id (INT, FOREIGN KEY references Medications(medication_id)),
    dosage (VARCHAR(50)),
    start_date (DATE),
    email(VARCHAR (225)),
    end_date (DATE),
    date_of_birth
ALTER TABLE Prescriptions DROP COLUMN Email(VARCHAR (225));
    date_of_birth
Constraints:
    start_date <= end_date (CHECK constraint), 
) ;  
CREATE TABLE Doctors (
    nurse_id (INT, PRIMARY KEY),
    first_name (VARCHAR(50), NOT NULL),
    last_name (VARCHAR(50), NOT NULL),
    specialization (VARCHAR(100)),
Constraints:
    specialization IS NOT NULL (CHECK constraint),
ALTER TABLE Doctors RENAME COLUMN nurse_id (INT, PRIMARY KEY), to new doctor_id(INT, PRIMARY KEY),
) ;
CREATE TABLE Sales (
    sale_id (INT, PRIMARY KEY),
    prescription_id (INT, FOREIGN KEY references Prescriptions(prescription_id)),
    sale_date (DATE, NOT NULL, DEFAULT current_date),
    total_price (DECIMAL(10,2), NOT NULL),
Constraints:
    total_price >= 0 (CHECK constraint),
) ;
